package com.example.msyo19.actividad5;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Itemc> items = new ArrayList<>();
    RecyclerView recyclerView;
    TextInputLayout nombre,apellido,edad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         nombre = findViewById(R.id.nombre);
        apellido = findViewById(R.id.apellido);
         edad = findViewById(R.id.edad);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView= findViewById(R.id.recycler);
        recyclerView.setLayoutManager(llm);

        Itemc jorge = new Itemc("jorge","carranza","21");
        items.add(jorge);
        Itemc juan = new Itemc("juan","Solis","12");
        items.add(juan);

        Adaptador adaptador= new Adaptador(items);
        recyclerView.setAdapter(adaptador);

        DividerItemDecoration deco = new DividerItemDecoration(this,llm.getOrientation());
        recyclerView.addItemDecoration(deco);

    }

    public void agregar(View view){
        String n,a,e;
        n = nombre.getEditText().getText().toString();
        a = apellido.getEditText().getText().toString();
        e = edad.getEditText().getText().toString();
        Itemc j = new Itemc(n,a,e);
        items.add(j);

        Adaptador adaptador= new Adaptador(items);
        recyclerView.setAdapter(adaptador);
    }
}
