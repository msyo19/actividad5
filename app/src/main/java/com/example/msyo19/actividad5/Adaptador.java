package com.example.msyo19.actividad5;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolderDatos> {
    @NonNull

    ArrayList<Itemc> items = new ArrayList<>();

    public Adaptador(ArrayList<Itemc> items) {
        this.items = items;
    }

    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item,null,false);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos viewHolderDatos, int i) {
        viewHolderDatos.asignar(items.get(i));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {
        TextView nombre,apellido,edad;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            nombre= itemView.findViewById(R.id.nombre);
            apellido= itemView.findViewById(R.id.apellido);
            edad= itemView.findViewById(R.id.edad);
        }

        public void asignar(Itemc itemc) {
            nombre.setText(itemc.getNombre());
            apellido.setText(itemc.getApellido());
            edad.setText(itemc.getEdad());
        }
    }
}
