package com.example.msyo19.actividad5;

public class Itemc {
    private String nombre;
    private String apellido;
    private String edad;

    public  Itemc(String n,String a,String e){
        this.nombre=n;
        this.apellido= a;
        this.edad = e;
    }


    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
